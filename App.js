import React from 'react';
import { View } from 'react-native';
import Map from './src/components/Map/Map';
import styles from './AppStyles'
import SearchMenu from './src/components/SearchMenu/SearchMenu';
import SelectItemsMenu from './src/components/SelectItemsMenu/SelectItemsMenu';
import DeliveredPage from './src/components/DeliveredPage/DeliveredPage'
import Header from './src/components/Header/Header';

class App extends React.Component {
  render() {
    const aaa = true
    return (
      <View style={styles.container}>
        <Header />
        <Map />
       {aaa ? <SearchMenu /> : <SelectItemsMenu />}
       {/* <DeliveredPage /> */}
      </View>
    );
  }
}
export default App
