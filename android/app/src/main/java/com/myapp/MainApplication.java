import com.airbnb.android.react.maps.MapsPackage;


 @Override
 protected List<ReactPackage> getPackages() {
        return Arrays.<ReactPackage>asList(
                new MainReactPackage(),
                new MapsPackage()
        );
    }