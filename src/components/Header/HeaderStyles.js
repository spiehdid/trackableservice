import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%'
    },
    imageMenu: {
        width: 30,
        height: 30
    },
    title: {
        color: 'red',
        fontWeight: 'bold',
        fontSize: 30
    }
});
export default styles