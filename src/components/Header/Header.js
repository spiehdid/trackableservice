import React from 'react'
import styles from './HeaderStyles'
import { View, Text, Image } from 'react-native';


class Header extends React.Component {
    render() {
        return (<View style={styles.container}>
            <Image
                style={styles.imageMenu}
                source={{ uri: 'https://st3.depositphotos.com/7889414/12824/v/950/depositphotos_128244112-stock-illustration-web-site-menu-icon-flat.jpg' }}
            />
            <Text style={styles.title}>vivi</Text>
            <Image
                style={styles.imageMenu}
                source={{ uri: 'https://st2.depositphotos.com/6628792/10286/v/950/depositphotos_102864408-stock-illustration-present-box-gift-icon.jpg' }}
            />
        </View>)
    }
}


export default Header