import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        height: '100%',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 20
    },
    image: {
        height: 90,
        width: 90,
        borderRadius: 50
    },
    imageRate: {
        height: 10,
        width: 10
    },
    imageVehicle: {
        height: 85,
        width: 140,
    },
    imageGetRate: {
        height: 40,
        width: 40,
        marginHorizontal: 5
    },
    rating: {
        flexDirection: 'row',
        paddingTop: 10
    },
    ratingContainer: {
        alignItems: 'center',
        marginVertical: 20,
        borderBottomWidth: 1,
        borderStyle: 'solid',
        borderBottomColor: 'grey',
        paddingBottom: 20
    },
    titlePage: {
        fontSize: 50,
        fontWeight: 'bold'
    },
    name: {
        fontSize: 18,
        fontWeight: 'bold'
    },
    groupButtons: {
        flexDirection: 'row'
    },
    button1: {
        width: 90,
        height: 50,
        backgroundColor: 'orange',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10
    },
    button2: {
        width: 90,
        height: 50,
        backgroundColor: '#6c45ec',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        marginHorizontal: 10
    },
    button3: {
        width: 90,
        height: 50,
        backgroundColor: '#452495',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10
    },
    buttonOther: {
        width: 290,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'grey',
        marginTop: 5
    },
    groupButtonText: {
        fontWeight: 'bold',
        fontSize: 18,
        color: 'white'
    },
    tipContainer: {
        alignItems: 'center'
    },
    tipTitle: {
        paddingBottom: 15
    },
    ButtonText: {
        color: 'red',
        fontSize: 18,
        fontWeight: 'bold'
    }
});
export default styles