import React from 'react'
import { View, Text, Image } from 'react-native';
import styles from './DeliveredPageStyles'


class SearchMenu extends React.Component {
  render() {
    const values = [1, 2, 3, 4, 5]
    return (<View style={styles.container}>
      <Text style={styles.titlePage}>Delivered!</Text>
      <Image
        style={styles.image}
        source={{ uri: 'https://do.ngs.ru/preview//do/671d756b17873dfde5cdfaa78e645b84_1465406974_750_1000.jpg' }}
      />
      <Text style={styles.name}>Ryan Urban</Text>
      <View style={styles.informarion}>
        <Text style={styles.informarionText}>Vivi/Bike - 4.8 <Image
          style={styles.imageRate}
          source={{ uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/Red_star.svg/231px-Red_star.svg.png' }}
        /> Rating</Text>
      </View>
      <Image
        style={styles.imageVehicle}
        source={{ uri: 'http://re-touch.ru/images/obtravka/1.jpg' }}
      />
      <View style={styles.ratingContainer}>
        <Text>Give Ryan a rating:</Text>
        <View style={styles.rating}>
          {values.map(el => <Image
            key={el}
            style={styles.imageGetRate}
            source={{ uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/Red_star.svg/231px-Red_star.svg.png' }}
          />)}
        </View>
      </View>
      <View style={styles.tipContainer}>
        <Text style={styles.tipTitle}>Do you want to tip Ryan?</Text>
        <View style={styles.tipButtons}>
        <View style={styles.groupButtons}>
          <View style={styles.button1}><Text style={styles.groupButtonText}>2$</Text></View>
          <View style={styles.button2}><Text style={styles.groupButtonText}>5$</Text></View>
          <View style={styles.button3}><Text style={styles.groupButtonText}>10$</Text></View>
        </View>
        <View style={styles.buttonOther}><Text style={styles.ButtonText}>OTHER</Text></View>
        </View>
      </View>
    </View>)
  }
}


export default SearchMenu