import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    map: {
        height: '65%',
        width: '100%'
    },
});
export default styles