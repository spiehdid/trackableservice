import React from 'react'
import MapView, { Marker } from 'react-native-maps';
import styles from './MapStyles'

class Map extends React.Component {
    render() {
        return (<MapView
            style={styles.map}
            region={{
                latitude: 47.2096768,
                longitude: 38.9431296,
                latitudeDelta: 0.015,
                longitudeDelta: 0.0121,
            }}
        >
            <Marker
                coordinate={{
                    latitude: 47.2096768,
                    longitude: 38.9431296
                }}
                title="here"
                description='i am'
            />
        </MapView>)
    }
}


export default Map