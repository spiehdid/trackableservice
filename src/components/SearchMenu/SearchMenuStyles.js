import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        height: '35%',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 20
    },
    inputContainer: {
        width: '80%',
        height: 90
    },
    input: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#f7f7f7',
        height: 50,
        paddingLeft: 15
    },
    inputTitle: {
        paddingBottom: 5
    },
    image: {
        height: 10,
        width: 10
    }
});
export default styles