import React from 'react'
import { View, Text, Image } from 'react-native';
import styles from './SearchMenuStyles'


class SearchMenu extends React.Component {
  render() {
    return (<View style={styles.container}>
      <View style={styles.inputContainer}>
        <Text style={styles.inputTitle}>Pick up adress</Text>
        <View style={styles.input}>
          <Image
            style={styles.image}
            source={{ uri: 'https://facebook.github.io/react-native/img/tiny_logo.png' }}
          />
          <Text>Enter pick up adress</Text>
        </View>
      </View>
      <View style={styles.inputContainer}>
        <Text style={styles.inputTitle}>Drop of adress</Text>
        <View style={styles.input}>
          <Image
            style={styles.image}
            source={{ uri: 'https://facebook.github.io/react-native/img/tiny_logo.png' }}
          />
          <Text>Enter dropoff adress</Text>
        </View>
      </View>
    </View>)
  }
}


export default SearchMenu