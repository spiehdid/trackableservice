import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        bottom: 0,
        backgroundColor: '#fff',
        height: '65%',
        width: '100%',
        padding: 20
    },
    nameContainer: {
        fontWeight: 'bold',
        fontSize: 20
    },
    popularContainer: {
        paddingTop: 20
    },
    itemsHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    popularHeader: {
        color: 'red',
        paddingBottom: 10
    },
    otherHeader: {
        paddingVertical: 20
    },
    popularContainerItems: {
        width: '100%',
        height: 'auto',
        padding: 10,
        backgroundColor: '#f7f7f7'
    },
    popularItem: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingBottom: 5
    },
    image: {
        width: 15,
        height: 15
    },
    otherImage: {
        transform: [{ rotate: "180 deg" }],
        width: 15,
        height: 15
    },
    buttonContainer: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '95%',
        height: 60,
        backgroundColor: 'black',
        borderRadius: 15
    },
    buttonTitle: {
        color: '#fff',
        fontWeight: 'bold'
    }
});
export default styles