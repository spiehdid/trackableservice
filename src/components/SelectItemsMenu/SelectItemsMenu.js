import React from 'react'
import { View, Text, Image } from 'react-native';
import styles from './SelectItemsMenuStyles'


class SelectItemsMenu extends React.Component {
  render() {
    const popularItems = ['Backpack', 'Laundry', 'Laptop/Computer', 'Bike', 'Gym Bog', 'Briefcose', 'Documents', 'Food', 'Luggoge']
    return (<View style={styles.container}>
      <Text style={styles.nameContainer}>What are you shipping?</Text>
      <View style={styles.popularContainer}>
        <View style={styles.itemsHeader}>
          <Text style={styles.popularHeader}>Popular items</Text>
          <Image
            style={styles.image}
            source={{ uri: 'https://image.flaticon.com/icons/png/512/130/130906.png' }}
          />
        </View>
        <View style={styles.popularContainerItems}>
          {popularItems.map(item =>
            <View style={styles.popularItem} key={item}>
              <Text style={styles.popularItemName}>{item}</Text>
              <View>
                <Text>Qty:1</Text>
              </View>
            </View>)}

        </View>
      </View>
      <View style={styles.itemsHeader}>
        <Text style={styles.otherHeader}>Other</Text>
        <Image
          style={styles.otherImage}
          source={{ uri: 'https://image.flaticon.com/icons/png/512/130/130906.png' }}
        />
      </View>
      <View style={styles.buttonContainer}>
        <View style={styles.button}>
          <Text style={styles.buttonTitle}>BOOK YOUR VIVI</Text>
          </View>
        </View>
    </View>)
  }
}


export default SelectItemsMenu